var browserSync    = require('browser-sync');
var reload         = browserSync.reload;
var gulp           = require('gulp');
var jshint         = require('gulp-jshint');
var concat         = require('gulp-concat');
var postcss        = require('gulp-postcss');
var sourcemaps     = require('gulp-sourcemaps');
var imagemin       = require('gulp-imagemin');
var gutil          = require('gulp-util');
var plumber        = require('gulp-plumber');
var inlineSource   = require('gulp-inline-source');
var postcssCalc    = require('postcss-calc');

// Gulp packages that duplicate work of Drupal libraries
// var uglifycss      = require('gulp-uglifycss');
// var uglifyjs       = require('uglify-js-harmony');
// var minifier       = require('gulp-uglify/minifier');

// Load all PostCSS processors
var processors = [
  require('postcss-import'),
  require('autoprefixer')({
    browsers: ['Firefox >= 40', 'ie >= 9', 'last 2 versions', '> 3%']
  }),
  require('postcss-each')({
    plugins: {
      beforeEach: [
        require('postcss-for'),
        require('postcss-mixins'),
        require('postcss-simple-vars')({silent: true}),
        postcssCalc({
          precision: 3
        }),
        require('postcss-hexrgba'),
        require('postcss-nested')
      ]
    }
  })

];

// CSS task
gulp.task('css', () => {

  return gulp.src([
      './src/css/_settings.css',
      './src/css/base.css',
      './src/css/layout.css',
      './src/css/module-*.css',
      './src/css/theme.css',
      './src/css/state.css',
    ])
    .pipe(plumber({
      errorHandler: (err) => {
        gutil.log(err.toString());
        this.emit('end');
      }
    }))
    .pipe(sourcemaps.init())
    // .pipe(concat('app.css'))
    .pipe(postcss(processors))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./dist/css'))
    .pipe(browserSync.stream());
});

gulp.task('css-deploy', () => {
  return gulp.src([
      './src/css/_settings.css',
      './src/css/base.css',
      './src/css/layout.css',
      './src/css/module-*.css',
      './src/css/theme.css',
      './src/css/state.css',
    ])
    .pipe(plumber({
      errorHandler: (err) => {
        gutil.log(err.toString());
        this.emit('end');
      }
    }))
    .pipe(postcss(processors))
    .pipe(gulp.dest('./dist/css'))
    .pipe(browserSync.stream());
});

gulp.task('ckeditor-css', () => {
  return gulp.src([
      './src/css/_settings.css',
      './src/css/base.css',
      './src/css/module-images.css',
      './src/css/ckeditor.css',
    ])
    .pipe(plumber({
      errorHandler: (err) => {
        gutil.log(err.toString());
        this.emit('end');
      }
    }))
    .pipe(concat('ckeditor.css'))
    .pipe(postcss(processors))
    .pipe(gulp.dest('./dist/css'))
    .pipe(browserSync.stream());
});

// JS task
gulp.task('scripts', () => {
  return gulp.src('./src/js/*.js')
    .pipe(gulp.dest('./dist/js'));
});

gulp.task('lint-scripts', () => {
  return gulp.src('./src/js/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

// Image opt task
gulp.task('images', () => {
  return gulp.src('./src/images/**/*')
    .pipe(imagemin())
    .pipe(gulp.dest('./dist/images'));
});

// Linting task

// Browser sync task for testing with index.html
gulp.task('browser-sync', () => {
  browserSync.init({
    server: "./",
    open: false
  });
});

// Process tasks
gulp.task('lint', ['lint-scripts']);

gulp.task('build', ['images', 'css-deploy', 'scripts']);

gulp.task('default', ['browser-sync'], () => {
  gulp.watch('src/images/**/*.*', ['images']).on('change', reload);
  gulp.watch(['src/css/*.css'], ['css', 'ckeditor-css']);
  gulp.watch(['index.html']).on('change', reload);
  gulp.watch('src/js/*.js', ['lint-scripts', 'scripts']).on('change', reload);
});
