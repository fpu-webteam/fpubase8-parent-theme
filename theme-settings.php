<?php

function fpubase8_form_system_theme_settings_alter(&$form, \Drupal\Core\Form\FormStateInterface &$form_state, $form_id = NULL) {
  // Work-around for a core bug affecting admin themes. See issue #943212.
  if (isset($form_id)) {
    return;
  }

  $form['fpubase8_social_links'] = [
    '#type' => 'fieldset',
    '#title' => t('Social links'),
    '#description' => t("Full URLs to each social network. Blank entries will be ignored."),

    // Facebook
    'facebook_url' => [
      '#type' => 'textfield',
      '#title' => t('Facebook'),
      '#default_value' => theme_get_setting('facebook_url'),
    ],

    // Instagram
    'instagram_url' => [
      '#type' => 'textfield',
      '#title' => t('Instagram'),
      '#default_value' => theme_get_setting('instagram_url'),
    ],

    // youtube
    'youtube_url' => [
      '#type' => 'textfield',
      '#title' => t('Youtube'),
      '#default_value' => theme_get_setting('youtube_url'),
    ],

    // Twitter
    'twitter_url' => [
      '#type' => 'textfield',
      '#title' => t('Twitter'),
      '#default_value' => theme_get_setting('twitter_url'),
    ],

    // Pinterest
    'pinterest_url' => [
      '#type' => 'textfield',
      '#title' => t('Pinterest'),
      '#default_value' => theme_get_setting('pinterest_url'),
    ],

  ];
}
